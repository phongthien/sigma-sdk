package sigma_sdk

import (
	"context"
	"encoding/json"
	"time"
)

type ClientRequest interface {
	Call(routingPath string, data any) ([]byte, error)
}

func (s *sigmaClient) Call(routingPath string, data any) ([]byte, error) {
	dataByte, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	channelResponse := make(chan ResponseData)

	s.routerChannel <- &RequestData{
		routingPath:     routingPath,
		channelResponse: channelResponse,
		data:            dataByte,
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(s.timeout)*time.Millisecond)
	defer cancel()

	select {
	case res := <-channelResponse:
		return res.data, res.error
	case <-ctx.Done():
		return nil, Timeout

	}

	return nil, err
}

func NewClient(routerChannel chan *RequestData, timeout int64) ClientRequest {
	return &sigmaClient{
		routerChannel: routerChannel,
		timeout:       timeout,
	}
}
