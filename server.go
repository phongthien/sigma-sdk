package sigma_sdk

type server struct {
	channel chan *RequestData
}

type Handler func(ctx Context)

func (s *server) Handle(h Handler) {

	for data := range s.channel {
		go h(data)
	}
}

type Server interface {
	Handle(h Handler)
}

func NewServer(channel chan *RequestData) Server {
	return &server{
		channel: channel,
	}
}
