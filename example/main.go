package main

import (
	"log"
	sigma_sdk "sigma-sdk"
	"time"
)

func main() {
	routerChanel := make(chan *sigma_sdk.RequestData)
	client := sigma_sdk.NewClient(routerChanel, 5000)

	server := sigma_sdk.NewServer(routerChanel)
	go server.Handle(func(ctx sigma_sdk.Context) {
		data, err := ctx.Request()
		log.Printf("server: %+v %+v", string(data), err)
		ctx.Response(struct {
			B string
		}{
			B: "b",
		}, err)

	})
	res, err := client.Call("dynamic.testPlugin", struct {
		A string `json:"a"`
	}{
		A: "a",
	})
	log.Printf("client: %+v %+v", string(res), err)
	time.Sleep(10 * time.Second)
}
