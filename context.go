package sigma_sdk

import (
	"encoding/json"
	"errors"
)

var Timeout = errors.New("time out")

type Context interface {
	Request() ([]byte, error)
	Response(data any, err error) error
}

type sigmaClient struct {
	routerChannel chan *RequestData
	timeout       int64
}

type RequestData struct {
	routingPath     string
	channelResponse chan ResponseData
	data            []byte
}

func (r RequestData) Request() ([]byte, error) {
	return r.data, nil
}

func (r RequestData) Response(data any, err error) error {
	if err != nil {
		r.channelResponse <- ResponseData{
			error: err,
		}
	}
	dataByte, err := json.Marshal(data)
	if err != nil {
		return err
	}
	r.channelResponse <- ResponseData{
		data: dataByte,
	}
	return nil
}

type ResponseData struct {
	error error
	data  []byte
}
